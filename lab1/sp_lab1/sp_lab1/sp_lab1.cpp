// sp_lab1.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"

#define DIV 1024 
#define WIDTH 7 



int main(int argc, _TCHAR* argv[])
{
	
	if (strcmp((char*)argv[1], "-e") == 0 && argc> 1)
	{
		MEMORYSTATUSEX statex;
		LPVOID lpMsg;
		DWORD dw = GetLastError();
		setlocale(0, "rus");
		statex.dwLength = sizeof(statex);

		GlobalMemoryStatusEx(&statex);

		_tprintf(TEXT("There is %*ld percent of memory in use.\n"),
			WIDTH, statex.dwMemoryLoad);
		_tprintf(TEXT("There are %*I64d total KB of physical memory.\n"),
			WIDTH, statex.ullTotalPhys / DIV);
		_tprintf(TEXT("There are %*I64d free KB of physical memory.\n"),
			WIDTH, statex.ullAvailPhys / DIV);
		_tprintf(TEXT("There are %*I64d total KB of paging file.\n"),
			WIDTH, statex.ullTotalPageFile / DIV);
		_tprintf(TEXT("There are %*I64d free KB of paging file.\n"),
			WIDTH, statex.ullAvailPageFile / DIV);
		_tprintf(TEXT("There are %*I64d total KB of virtual memory.\n"),
			WIDTH, statex.ullTotalVirtual / DIV);
		_tprintf(TEXT("There are %*I64d free KB of virtual memory.\n"),
			WIDTH, statex.ullAvailVirtual / DIV);

		_tprintf(TEXT("There are %*I64d free KB of extended memory.\n"),
			WIDTH, statex.ullAvailExtendedVirtual / DIV);
		LPTSTR pszBuf = NULL;

		pszBuf = (LPTSTR)LocalAlloc(
			LPTR,
			INT_MAX);
		// Handle error condition 
		if (pszBuf == NULL)
		{
			dw = GetLastError();
			_tprintf(TEXT("LocalAlloc failed (%d)\n"), GetLastError());
			FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
				FORMAT_MESSAGE_FROM_SYSTEM |
				FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				dw,
				MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
				(LPTSTR)&lpMsg,
				0,
				NULL);
			_tprintf(TEXT("%s"), lpMsg);
			return 0;
		}

		_tprintf(TEXT("LocalAlloc allocated %d bytes\n"), LocalSize(pszBuf));

		LocalFree(pszBuf);

		void WINAPI GetSystemInfo(
			_Out_ LPSYSTEM_INFO lpSystemInfo
		);
		return 0;
	}
	else if (strcmp((char*)argv[1], "-s") == 0 && argc > 1)
	{
		SYSTEM_INFO siSysInfo;
		GetSystemInfo(&siSysInfo);

		printf("Hardware information: \n");
		printf("  OEM ID: %u\n", siSysInfo.dwOemId);
		printf("  Number of processors: %u\n",siSysInfo.dwNumberOfProcessors);
		printf("  Page size: %u\n", siSysInfo.dwPageSize);
		printf("  Processor type: %u\n", siSysInfo.dwProcessorType);
		printf("  Minimum application address: %lx\n",siSysInfo.lpMinimumApplicationAddress);
		printf("  Maximum application address: %lx\n",siSysInfo.lpMaximumApplicationAddress);
		printf("  Active processor mask: %u\n",siSysInfo.dwActiveProcessorMask);
	}
	else
	{
		printf("Err of Arguments comand sting!\n");
	}		
}

