#include "stdafx.h"

void checkError(DWORD dw);

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(0, "rus");
	wprintf(L"From -> %s\n", argv[2]);
	wprintf(L"To -> %s\n", argv[3]);
	LPCWSTR pathToFile = argv[2];

	DWORD dw;
	char dataBufferANSI[4097];
	wchar_t dataBufferUNICODE[4097];
	LPVOID lpMsg;
	DWORD dwNumOfBytes = 4096;
	DWORD lpDwNumOfBytes;
	HANDLE hFile = CreateFile(pathToFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, NULL, 0);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		
		dw = GetLastError();
		checkError(dw);
		return 0;
	}
	else
	{

		if(wcscmp(L"-u", argv[1]) == 0)
		{
			do
			{
				memset(dataBufferANSI, 0, 4097);
				ReadFile(hFile, dataBufferANSI, dwNumOfBytes, &lpDwNumOfBytes, NULL);
				puts("ANSI-file:");
				if (lpDwNumOfBytes > 0)
					printf("%s", dataBufferANSI);
			} while (lpDwNumOfBytes == 4096);

			puts("\n<------------>");
			memset(dataBufferUNICODE, 0, 4097);
	
			MultiByteToWideChar(GetACP(), 0, dataBufferANSI, -1, dataBufferUNICODE, 4097);

			HANDLE newFile = CreateFile(argv[3], GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, NULL, 0);

			WriteFile(newFile, dataBufferUNICODE, wcslen(dataBufferUNICODE) * 2, &lpDwNumOfBytes, NULL);
			puts("Unicode-file:");
			if (lpDwNumOfBytes > 0)
				_tprintf(L"%s\n", dataBufferUNICODE);
			printf("Success to Unicode!\n");
		}
		else if(wcscmp(argv[1], L"-a") == 0)
		{
			do
			{
				memset(dataBufferUNICODE, 0, 4097);
				ReadFile(hFile, dataBufferUNICODE, dwNumOfBytes, &lpDwNumOfBytes, NULL);
				puts("Unicode-file:");
				if (lpDwNumOfBytes > 0)
				{
				wprintf(L"%s\n", dataBufferUNICODE + 1);
				}
				
			} while (lpDwNumOfBytes == 4096);

			puts("\n");
			memset(dataBufferANSI, 0, 4096);
			WideCharToMultiByte(CP_ACP, 0, dataBufferUNICODE + 1 , 4097, dataBufferANSI, 4096, 0, NULL);

			HANDLE newFile = CreateFile(argv[3], GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, NULL, 0);

			WriteFile(newFile, dataBufferANSI, strlen(dataBufferANSI), &lpDwNumOfBytes, NULL);
			puts("ANSI-file:");
			if (lpDwNumOfBytes > 0)
				printf("%s\n", dataBufferANSI);
			printf("Success to ANSI!\n");
		}
		else {
			printf("Err of Arguments comand sting!\n");
		}
	}
	return 0;
}

void checkError(DWORD dw){
	LPVOID lpMsg;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsg,
		0,
		NULL);
	_tprintf(TEXT("%s"), lpMsg);
}

